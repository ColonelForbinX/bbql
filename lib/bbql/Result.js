/**
 * Utility collection class for handling BBQL Query results.
 * @package bbql
 */
export default class Result {

  /**
   * Constructor.
   *
   * @param array rows
   *   an array of JSON objects returned from a Query
   * @return Result this
   */
  constructor(rows = []) {
    this.rows = rows;
    return this;
  }

  /**
   * Reduce a Result set to an array of field values.
   *
   * @param string key
   *   the name of the field to pluck
   * @param string value
   *   an optional second field to pluck as key => value
   * @return mixed
   */
  pluck(key, value = null) {
    let plucked;

    if (value) {
      plucked = {};
      for (let r in this.rows) {
        plucked[this.rows[r][key]] = this.rows[r][value];
      }
    } else {
      plucked = [];
      for (let r in this.rows) {
        plucked.push(this.rows[r][field]);
      }
    }

    return plucked;
  }

  /**
   * Return the first row in the result set.
   *
   * @return object
   */
  first() {
    return this.rows[0] || null;
  }

  /**
   * Array shift() result rows until empty.
   *
   * @param int index
   *   the index of the record to retrieve
   * @return object
   */
  get(index = null) {
    return (index != null)
      ? this.rows[index]
      : this.rows.shift();
  }

  /**
   * Return the number of rows in the result set.
   *
   * @return int
   */
  count() {
    return this.rows.length;
  }

  /**
   * Return a JSON string representation of a result set.
   *
   * @param int index
   *   if provided, only return a single result
   * @return string|null
   */
  stringify(index = null) {
    return (index != null)
      ? JSON.stringify(this.rows[index])
      : JSON.stringify(this.rows);
  }
}