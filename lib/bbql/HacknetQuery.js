import Query from '/lib/bbql/Query.js';

/**
 * Utility class for querying BitBurner hacknet nodes using BBQL.
 * @package bbql
 */
export default class HacknetQuery extends Query {

  /**
   * Build list of all hacknet nodes w/ metadata.
   *
   * @return object
   *   a JSON object keyed by node name
   */
  _buildDb() {
    let nodes = {};

    for (let n = 0; n < this.ns.hacknet.numNodes(); n++) {
      let node = this.ns.hacknet.getNodeStats(n);

      node.id = n;
      node.levelUpgradeCost = this.ns.hacknet.getLevelUpgradeCost(n);
      node.ramUpgradeCost = this.ns.hacknet.getRamUpgradeCost(n);
      node.coreUpgradeCost = this.ns.hacknet.getCoreUpgradeCost(n);

      nodes[n] = node;
    }

    return nodes;
  }
}