import Result from '/lib/bbql/Result.js';

/**
 * Utility class for querying arrays of generic BitBurner objects.
 * Extending classes must implement _buildDb().
 * @package bbql
 */
export default class Query {

  /**
   * Constructor.
   *
   * @param object ns
   *   the netscript namespace object
   * @return Query this
   */
  constructor(ns) {
    this.ns = ns;
    this.db = this._buildDb();
    this.fields = [];
    this.conditions = [];
    this.orderField = null;
    this.orderDir = 'ASC';
    this.max = null;
    return this;
  }

  /**
   * Build list of all rows in the db.
   *
   * @return object
   *   a JSON object keyed by hostname
   */
  _buildDb() {
    return {};
  }

  /**
   * Specify fields to pull for result rows.
   *
   * @param array fields
   *   either an array of strings like:
   *   ['hostname', 'hackDifficulty']
   *   or an array of arrays with [field, alias] pairs like:
   *   [['hostname', 'name'], ['hackDifficulty', 'security']]
   * @return Query this
   */
  select(fields) {
    this.fields = fields;
    return this;
  }

  /**
   * Add a condition to the query.
   *
   * @param string field
   *   the field name on which the condition is based
   * @param string op
   *   an operator (=, !=, >, >=, <, <=)
   * @param mixed value
   *   the comparison value
   * @return Query this
   */
  where(field, op, value = null) {
    this.conditions.push([field, op, value]);
    return this;
  }

  /**
   * Set the sort order field and direction for the query.
   *
   * @param mixed field
   *   string: the field name to sort on
   *   function: an (a, b) comparison f(x) to pass to sort()
   * @param string dir
   *   the sort direction (ASC or DESC)
   * @return Query this
   */
  orderBy(field, dir = 'ASC') {
    this.orderField = field;
    this.orderDir = dir;
    return this;
  }

  /**
   * Set the limit for this query.
   *
   * @param int max
   *   the maximum number of records to return
   * @return Query this
   */
  limit(max) {
    this.max = max;
    return this;
  }

  /**
   * Process a query into an array of result JSON rows.
   *
   * @return array
   */
  execute() {
    let matches = [];

    // Loop through database
    for (let r in this.db) {
      let row = this.db[r];
      let failCount = this.conditions.length;

      // Evaluate conditions.
      for (let c in this.conditions) {
        let field = this.conditions[c][0];
        let op = this.conditions[c][1];
        let value = this.conditions[c][2];

        switch (op) {
          case '=': if (row[field] == value) failCount--; break;
          case '!=': if (row[field] !== value) failCount--; break;
          case '>': if (row[field] > value) failCount--; break;
          case '>=': if (row[field] >= value) failCount--; break;
          case '<': if (row[field] < value) failCount--; break;
          case '<=': if (row[field] <= value) failCount--; break;
        }
      }

      if (failCount == 0) matches.push(row);
    }

    // Sort results by orderField/orderDir.
    if (this.orderField) {
      // Functions get passed directly to sort.
      if (this.orderField instanceof Function) {
        matches.sort(this.orderField);
      }
      // Othewise use orderField and orderDir.
      else {
        matches.sort((a, b) => {
          switch (this.orderDir.toUpperCase()) {
            case 'ASC':
              return (a[this.orderField] > b[this.orderField]) ? 1 : -1;
              break;
            case 'DESC':
              return (a[this.orderField] < b[this.orderField]) ? 1 : -1;
              break;
          }
        });
      }
    }

    // Reduce to selected fieldset.
    if (this.fields.length) {
      for (let m in matches) {
        let filtered = {};

        for (let f in this.fields) {
          let field = this.fields[f];

          // Arrays are mappings from original field => aliased field.
          if (Array.isArray(field)) {
            let orig = field[0];
            let alias = field[1];
            filtered[alias] = matches[m][orig];
          }
          // Strings are just straight field mappings.
          else {
            filtered[field] = matches[m][field];
          }
        }

        matches[m] = filtered;
      }
    }

    // Limit results.
    if (this.max) matches.length = this.max;

    return new Result(matches);
  }
}