import Query from '/lib/bbql/Query.js';

/**
 * Utility class for querying BitBurner servers using BBQL.
 * @package bbql
 */
export default class ServerQuery extends Query {

  /**
   * Build list of all server targets w/ metadata.
   *
   * @param object servers
   *   the recursive list
   * @param string server
   *   the server being scanned/parsed
   * @return object
   *   a JSON object keyed by hostname
   */
  _buildDb(servers = {}, server = 'home') {
    if (servers[server]) return servers;

    servers[server] = this.ns.getServer(server);

    let children = this.ns.scan(server);
    for (let c in children) this._buildDb(servers, children[c]);

    return servers;
  }
}