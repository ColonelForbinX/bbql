# BBQL

BBQL is a simple query library for building lists of BitBurner objects using
functions familiar to anyone that has used a database abstraction layer for
something like SQL.

Example usage:

```
/** @param {NS} ns **/
import ServerQuery from '/lib/bbql/ServerQuery.js';

export async function main(ns) {
  let targets = new ServerQuery(ns)
    .select(['hostname', 'hackDifficulty', 'moneyAvailable'])
    .where('hackDifficulty', '<', 25.0)
    .where('backdoorInstalled', '=', true)
    .orderBy('moneyAvailable', 'DESC')
    .limit(5)
    .execute();

  ns.tprint(targets.rows);
}
```

Which will yield output similar to:

```
[
  {
    "hostname": "phantasy",
    "hackDifficulty": 20,
    "moneyAvailable": 24000000
  },
  {
    "hostname": "max-hardware",
    "hackDifficulty": 15,
    "moneyAvailable": 10000000
  },
  {
    "hostname": "harakiri-sushi",
    "hackDifficulty": 15,
    "moneyAvailable": 4000000
  },
  {
    "hostname": "hong-fang-tea",
    "hackDifficulty": 15,
    "moneyAvailable": 3000000
  },
  {
    "hostname": "nectar-net",
    "hackDifficulty": 20,
    "moneyAvailable": 2750000
  }
]
```

# Bitburner

[![Build Status](https://github.com/danielyxie/bitburner/actions/workflows/ci.yml/badge.svg?branch=dev)](https://github.com/danielyxie/bitburner/actions/workflows/ci.yml)

Bitburner is a programming-based [incremental game](https://en.wikipedia.org/wiki/Incremental_game)
that revolves around hacking and cyberpunk themes.
The game can be played at https://danielyxie.github.io/bitburner or installed through [Steam](https://store.steampowered.com/app/1812820/Bitburner/).

See the [frequently asked questions](./FAQ.md) for more information . To discuss the game or get help, join the [official discord server](https://discord.gg/TFc3hKD)